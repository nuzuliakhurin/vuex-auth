import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const moduleA = {
  namespaced: true,
  state: {
    data: {
      name: [
        {
          id: 1,
          name: 'Axel Andrian'
        },
        {
          id: 2,
          name: 'Mirza Zanuar'
        }
      ],
      dataUser: {
        username: '',
        password: ''
      },
      count: 0,
      users: []
    }
  },
  mutations: {
    increment(state) {
      state.data.count++
    },
    setUser(state, payload) {
      state.data.users = payload
    },
    setUsername(state, username) {
      state.data.username = username
    },
    setPassword(state, password) {
      state.data.password = password
    }
  },
  actions: {
    getUser({ commit }) {
      axios.get('https://jsonplaceholder.typicode.com/users/')
        .then((response) => {
          commit('setUser', response.data)
        })
    },
    login() {
      axios({
        method: 'post',
        url: 'http://10.30.30.31:8000/api/login',
        data: {
          username: this.state.data.username,
          password: this.state.data.password
        }
      })
    }
  },
  getters: {
    usersCount(state) {
      return state.data.users.length
    }
  }
}

export default new Vuex.Store({
  modules: {
    a: moduleA
  }
})
